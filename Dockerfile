FROM python:3.10.12
WORKDIR /app
COPY . /app
RUN pip3 install  --upgrade pip
RUN pip3 install -v -r requirements.txt
CMD ["python3", "./provider-service.py"]
