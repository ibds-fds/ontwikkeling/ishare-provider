import http.client as http_client
import json
import logging
import uuid
from typing import Annotated

import requests
from fastapi import FastAPI, Form, HTTPException, Request
from fastapi_healthcheck import HealthCheckFactory, healthCheckRoute
from python_ishare import create_jwt

from provider.config import *

app = FastAPI()

# HTTP request Logging
http_client.HTTPConnection.debuglevel = 1
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

# Add Health Checks
_healthChecks = HealthCheckFactory()
app.add_api_route('/health', endpoint=healthCheckRoute(factory=_healthChecks))


def token(target_id=None, token_id=None, capabilities: json = None):
    payload = {
        "iss": id,
        "sub": id,
        "jti": token_id or str(uuid.uuid4())
    }
    if target_id:
        payload["aud"] = target_id
    if capabilities:
        payload['capabilities_info'] = capabilities
    return create_jwt(
        payload=payload,
        private_key=private_key,
        x5c_certificate_chain=certificate_chain
    )


fixed_fake_token = token()


@app.get("/")
def read_root():
    return token


@app.post("/connect/token")
def authenticate_token(grant_type: Annotated[str, Form()],
                       scope: Annotated[str, Form()],
                       client_id: Annotated[str, Form()],
                       client_assertion_type: Annotated[str, Form()],
                       client_assertion: Annotated[str, Form()]):
    requests_log.info(
        f'Authenticate token: \n\tgrant_type={grant_type}, \n\tscope={scope}, \n\tclient_id={client_id}, \n\tclient_assertion_type={client_assertion_type}, \n\tclient_assertion={client_assertion}')
    if grant_type != 'client_credentials':
        requests_log.info(f'Wrong grant_type: {grant_type}')
        raise HTTPException(status_code=400, detail="Wrong grant_type")
    if scope != 'iSHARE':
        requests_log.info(f'Wrong scope: {scope}')
        raise HTTPException(status_code=400, detail="Wrong scope")
    if client_assertion_type != 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer':
        requests_log.info(f'Wrong client_assertion: {client_assertion}')
        raise HTTPException(status_code=400, detail="Wrong client_assertion")
    if client_id != consumer_id:
        requests_log.info(f'Unknown client_id: {client_id}')
        raise HTTPException(status_code=401)

    return {'access_token': fixed_fake_token}


@app.get("/capabilities")
def capabilities(req: Request):
    requests_log.info('capabilities')
    # check access token
    capabilities = \
        {
            'capabilities_info':
                {'party_id': 'EU.EORI.NL002FDSTEST',
                 'ishare_roles': ['Service Provider'],
                 'supported_versions': [
                     {
                         'version': '1.0',
                         'supported_features': [
                             {
                                 'public': [
                                     {
                                         'id': '10000',
                                         'feature': 'capabilities',
                                         'description': 'Retrieves iSHARE capabilities',
                                         'url': 'https://my-address/capabilitie',
                                         'token_endpoint': 'https://my-address/connect/token'
                                     }]
                             }
                         ]
                     }
                 ]}
        }
    if valid_token(req):
        capabilities['capabilities_info']['supported_versions'][0]['supported_features'][0]['restricted'] = [
            {
                'id': '20000',
                'feature': 'list parkeerrechten',
                'description': 'Lists all the parkeerrechten',
                'url': 'https://my-address/parkeerrechten'
            }]
    # tokenize json return value
    return {'capabilities_token': token(capabilities=capabilities)}


@app.get("/parkeerrechten")
def parkeerrechten_list(req: Request):
    requests_log.info('(parkeerrechten) list')
    # check access token
    if (valid_token(req)):
        response = requests.get(f"http://parkeerrechten-api-svc/parkeerrechten/?format=json")
        return response.json()
    else:
        raise HTTPException(status_code=401)


def get_token_json(base_url, server_id):
    return requests.post(f"{base_url}/connect/token",
                         headers={
                             'Content-Type': 'application/x-www-form-urlencoded'
                         },
                         data={
                             'grant_type': 'client_credentials',
                             'scope': 'iSHARE',
                             'client_id': id,
                             'client_assertion_type': 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
                             'client_assertion': token(server_id)
                         }).json()

def valid_token(req: Request):
    requests_log.info(f'headers: {req.headers}')
    if "Authorization" in req.headers:
        token = req.headers["Authorization"]
        return f'Bearer {fixed_fake_token}' == token
    else:
        return False


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=80)
