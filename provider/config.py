import os

from cryptography.x509 import load_pem_x509_certificates
from cryptography.hazmat.primitives.serialization import load_pem_private_key

satellite_base_url = os.environ.get('SATELLITE_BASE_URL', 'https://sat-mw.uat.isharetest.net')
private_key_string = os.environ.get('PROVIDER_PRIVATE_KEY')
private_key = load_pem_private_key(bytes(private_key_string, 'UTF-8'), password=None)
certificate_chain_string = os.environ.get('PROVIDER_CERTIFICATE_CHAIN')
certificate_chain = load_pem_x509_certificates(bytes(certificate_chain_string, 'UTF-8'))
satellite_certificate_chain_string = os.environ.get('SATELLITE_CERTIFICATE_CHAIN')
satellite_certificate_chain = load_pem_x509_certificates(bytes(satellite_certificate_chain_string, 'UTF-8'))
consumer_id = "EU.EORI.NL001FDSTEST"
id = "EU.EORI.NL003FDSTEST,"
satellite_id = "EU.EORI.NL000000000"